import java.util.Iterator;
import java.util.LinkedList;
import java.util.ArrayList;

public class HashTable {
  private final int DEFAULT_TABLE_SIZE = 17;
  private ArrayList<LinkedList<Student>> array = null;

  HashTable(){
    array = new ArrayList<LinkedList<Student>>(DEFAULT_TABLE_SIZE);
    for(int i = 0; i < DEFAULT_TABLE_SIZE; i++){
      array.add(null);
    }
  }

  // 将int类型的key哈希到array的某个索引
  public int hash(int key){
  }

  // 将stu放入哈希表
  public void put(Student stu){
  }

  // 根据key取出某个Student的信息，这里key用Studeng.id
  public Student get(int key){
  }

  // 将整个哈希表打印出来，方便核对
  public void print(){

  }
}