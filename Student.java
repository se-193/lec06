public class Student {
  public int id;
  public String name;
  public String birthday;
  public String sex;

  Student(int id, String name, String birthday, String sex){
    this.id = id;
    this.name = name;
    this.birthday = birthday;
    this.sex = sex;
  }

  public String toString(){
    return Integer.toString(id) + ' ' + name + ' ' + birthday + ' ' + sex;
  }
}